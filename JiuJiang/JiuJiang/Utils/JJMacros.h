//
//  JJMacros.h
//  JiuJiang
//
//  Created by wuming on 14-9-29.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IOS_7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_GREAT_IOS_7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_IOS_6 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
#define IS_RETINA ([[UIScreen mainScreen] scale]==2)
#define IS_IPHONE_5_SCREEN_SIZE ([UIScreen mainScreen].bounds.size.height == 568.0f)

#define ONE_PIXEL_WIDTH (1.0 / [UIScreen mainScreen].scale)


#define APPDELEGATE [[UIApplication sharedApplication]delegate]



