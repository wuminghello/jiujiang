//
//  AppDelegate.m
//  JiuJiang
//
//  Created by wuming on 14-9-24.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import "AppDelegate.h"
#import "JJMacros.h"
#import "JJBaseTabbarController.h"

#import "HomeViewController.h"
#import "FindViewController.h"
#import "ComposeViewController.h"
#import "SettingViewController.h"
#import "LifeViewController.h"

#import "JJBaseViewController.h"

@interface AppDelegate ()
@property (nonatomic,strong) JJBaseTabbarController *baseTabbarController;

@property (nonatomic,strong) HomeViewController *homeViewController;
@property (nonatomic,strong) FindViewController *findViewController;
@property (nonatomic,strong) ComposeViewController *composeViewController;
@property (nonatomic,strong) SettingViewController *settingViewController;
@property (nonatomic,strong) LifeViewController *lifeViewController;
@property (nonatomic,strong) JJBaseViewController *baseViewController;
@end

@implementation AppDelegate

-(void)initTabViewControllers{
   // self.baseTabbarController = [[UITabBarController alloc] init];
    
    // 顶部导航栏通用属性设定-------------------
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary:[[UINavigationBar appearance] titleTextAttributes]];
    [titleBarAttributes setValue:[UIFont systemFontOfSize:18] forKey:UITextAttributeFont];
    [titleBarAttributes setValue:[UIColor blackColor]  forKey:UITextAttributeTextColor];
    [titleBarAttributes setValue:[NSValue valueWithUIOffset:UIOffsetZero] forKey:UITextAttributeTextShadowOffset];
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
    
    NSMutableDictionary *barItemAttributes = [NSMutableDictionary dictionaryWithDictionary:[[UIBarButtonItem appearance] titleTextAttributesForState:UIControlStateNormal]];
    [barItemAttributes setValue:[UIFont systemFontOfSize:14] forKey:UITextAttributeFont];
    [barItemAttributes setValue:[UIColor colorWithRed:0.9020 green:0.1804 blue:0.0157 alpha:1.0000] forKey:UITextAttributeTextColor];
    [barItemAttributes setValue:[NSValue valueWithUIOffset:UIOffsetZero] forKey:UITextAttributeTextShadowOffset];
    [[UIBarButtonItem appearance] setTitleTextAttributes:barItemAttributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:barItemAttributes forState:UIControlStateHighlighted];
    
    // HomePage---------------------------
   // AIFUIViewController *homePageViewController;
    self.homeViewController = [[HomeViewController alloc] init];
    
    UINavigationController *homePageNavigationController = [[UINavigationController alloc] initWithRootViewController:self.homeViewController];
    
    
    if (IS_IOS_7) {
        homePageNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[[UIImage imageNamed:@"tabbar_home.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[UIImage imageNamed:@"tabbar_home_selected.png"]];
    }else{
        UITabBarItem *homeBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:nil tag:0];
        [homeBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_home_selected.png"]
                  withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_home_selected.png"]];
        homePageNavigationController.tabBarItem = homeBarItem;
    }
    
    // Search---------------------------
    self.findViewController = [[FindViewController alloc] init];
    UINavigationController *findNavigationController = [[UINavigationController alloc] initWithRootViewController:self.findViewController];
    
    if (IS_IOS_7) {
        findNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"发现" image:[[UIImage imageNamed:@"tabbar_search.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[UIImage imageNamed:@"tabbar_search_selected.png"]];
    }else{
        UITabBarItem *searchBarItem = [[UITabBarItem alloc] initWithTitle:@"发现" image:nil tag:1];
        [searchBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_search_selected.png"]
                    withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_search_selected.png"]];
        findNavigationController.tabBarItem = searchBarItem;
    }
    
    
    // Cart---------------------------
    // 先创建NavVC，购物车VC稍后创建
    self.composeViewController = [[ComposeViewController alloc] init];
  
    
    UINavigationController *composeNavigationController = [[UINavigationController alloc] initWithRootViewController:self.composeViewController];
    
    if (IS_IOS_7) {
        self.composeViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Compose" image:[[UIImage imageNamed:@"tabbar_cart.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[UIImage imageNamed:@"tabbar_cart_selected.png"]];
    }else{
        UITabBarItem *cartBarItem = [[UITabBarItem alloc] initWithTitle:@"Compose" image:nil tag:2];
        [cartBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_cart_selected.png"]
                  withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_cart_selected.png"]];
        composeNavigationController.tabBarItem = cartBarItem;
    }
    
    
    
    self.lifeViewController = [[LifeViewController alloc]init];
    UINavigationController *lifeNavigationController = [[UINavigationController alloc] initWithRootViewController:self.lifeViewController];
    
    if (IS_IOS_7) {
        lifeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"生活馆" image:[[UIImage imageNamed:@"tabbar_user.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[UIImage imageNamed:@"tabbar_user_selected.png"]];
    }else{
        UITabBarItem *orderBarItem = [[UITabBarItem alloc] initWithTitle:@"生活馆" image:nil tag:3];
        [orderBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_user_selected.png"]
                   withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_user_selected.png"]];
        lifeNavigationController.tabBarItem = orderBarItem;
    }
    
    
    
    // MSG---------------------------
    self.settingViewController = [[SettingViewController alloc] init];
    UINavigationController *moreNavigationController = [[UINavigationController alloc] initWithRootViewController:self.settingViewController];
    if (IS_IOS_7) {
        moreNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"设置" image:[[UIImage imageNamed:@"tabbar_more.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[UIImage imageNamed:@"tabbar_more_selected.png"]];
    }else{
        UITabBarItem *moreBarItem = [[UITabBarItem alloc] initWithTitle:@"设置" image:nil tag:4];
        [moreBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_more_selected.png"]
                  withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_more_selected.png"]];
        moreNavigationController.tabBarItem = moreBarItem;
    }
    
    
    
    
    [self.baseTabbarController setViewControllers:[NSMutableArray arrayWithObjects:
                                        homePageNavigationController,
                                        findNavigationController,
                                        composeNavigationController,
                                        lifeNavigationController,
                                        moreNavigationController,
                                        nil]];
    
    if (IS_IOS_7) {
        self.baseTabbarController.tabBar.tintColor = [UIColor redColor];
    }
    else {
        [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
        self.baseTabbarController.tabBar.backgroundImage = [[UIImage imageNamed:@"sort-bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch];
    }
    
    if ([self.window respondsToSelector:@selector(setRootViewController:)]) {
        self.window.rootViewController = self.baseTabbarController;
    } else {
        [self.window addSubview:self.baseTabbarController.view];
    }
    
    [self.window setRootViewController:self.baseTabbarController];
    [self.window makeKeyAndVisible];
    // 获取购物车tab badge
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    //设置顶部状态栏-----------------------
    if (IS_IOS_7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    }
    self.baseTabbarController = [[JJBaseTabbarController alloc] init];
    
    [self initTabViewControllers];
    
    return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
