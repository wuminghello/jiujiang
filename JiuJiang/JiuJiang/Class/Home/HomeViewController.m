//
//  HomeViewController.m
//  JiuJiang
//
//  Created by wuming on 14-10-8.
//  Copyright (c) 2014年 wuming. All rights reserved.
//

#import "HomeViewController.h"
#import "fistPage.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    fistPage *firstPage = [[fistPage alloc] init];
    
    [firstPage testingService];
    //[firstPage testingService2];
    
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
