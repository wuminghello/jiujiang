Pod::Spec.new do |s|
    s.name = 'jjsdk'
    s.version = '0.1.0'
    s.license = 'MIT'
    s.summary = '[summary]'
    s.homepage = 'http://51tangsengrou.com'
    s.description = 'jjsdk'
    s.author = {'Wu ming' => 'wuminghello@gmail.com'}
    s.source = { :git => 'https://wuminghello@bitbucket.org/wuminghello/jjsdk.git'}
    s.platform = :ios,'6.0'
    s.source_files  = "JJSDK/**/*.{h,m}", "Resources/Frameworks/*.framework/Headers/*.h"
    s.dependency 'tsrhttpsdk', '0.1.0'

end
